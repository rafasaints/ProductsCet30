﻿namespace ProductsCet30.Domain
{
    using System.Data.Entity;
    public class DataContext:DbContext
    {
        public DataContext():base("DefaultConnection")
        {
            
        }

        public DbSet<ProductsCet30.Domain.Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }
    }
}
