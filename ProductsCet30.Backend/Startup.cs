﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProductsCet30.Backend.Startup))]
namespace ProductsCet30.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
